﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bookie_stats
{
    /// <summary>
    /// To be done. This class gonna find best matches for result: win, draw, lose.
    /// I assume that, the biggest probability of winning is to search only matches where hosts
    /// mostly win at home, and visitors mostly lose playing away.
    /// To achive that, I'm gonna save matches if home winners were equal or greater than sum of theirs draws and losses
    /// and visitors losses were equal of greater than sum of theirs draws and winners.
    /// </summary>
    class mainType : PredictionsByUserInput
    {

        protected override bool MatchSelections(string content)
        {
            // Shouldn't be here, but never mind. Ugly regex searches for the Bet-At-Home stakes.
            Match odds = Regex.Match(content, @"Bet-at-home.*?(efefef;'>|<b>)+(.*?)<.*?(efefef;'>|<b>)+(.*?)<.*?(efefef;'>|<b>)+(.*?)<.*?(<td >|<b>)+(.*?)<.*?(<td >|<b>)+(.*?)<",
                                        RegexOptions.IgnoreCase);

            string tempWin = odds.Groups[2].Value;
            if (tempWin == "&nbsp;" || tempWin == "") tempWin = "Stake: NULL";
            else winOdds = string.Format("Stake: {0}", tempWin);


            bool select = false;

            // Throws out all cups etc.
            foreach (Match elimination in CompetitionSelect.Matches(content))
            {
                if (cups.IsMatch(elimination.Groups[1].ToString()))
                {
                    select = true;
                    break;
                }
            }
            return select;
        }

        protected override void ShowStats()
        {
            // string for the file statistics
            string toFile = "";
            int hits = 0;
            string filePathResult = filePath;
            filePathResult += string.Format(@"\predictions_score {0}.txt", date);

            using (StreamWriter outputFile = File.CreateText(filePathResult))
            {
                foreach (var stats in MatchIDs)
                {
                    message += stats.Key + "\n" + stats.Value + "\n";
                    toFile += stats.Key + Environment.NewLine + stats.Value.Substring(0, 1).ToString() + Environment.NewLine;

                    hits++;
                }
                if (message == "")
                {
                    throw new Exception("Bad luck. Zero good matches!");
                }
                else
                    outputFile.WriteLine(toFile);
            }
            Sendmail.SendingPredictions(message, date, hits);
        }

        protected override string OverUnderStats(string H2H, string[] clubsID)
        {
            Regex lastMatch = new Regex(@"[0-9]{2}\.[0-9]{2}\.[0-9]{4}", RegexOptions.IgnoreCase);
            Regex result = new Regex(@"showClubMenu(.*?)class='?(.*?)'.*?class='?(.*?)'(.*?)'", RegexOptions.IgnoreCase);

            int homeWin = 0;
            int homeDraw= 0;
            int homeLose = 0;
            int visitorsWin = 0;
            int visitorsDraw = 0;
            int visitorsLose = 0;
            int HomeCount = 0;
            int VisitorsCount = 0;

            string HomeLastDate = "";
            string VisitorsLastDate = "";
            string HomeCups = "";
            string VisitorsCups = "";
            probability = 0.0f;

            for (int i = 0; i <= 1; i++)
            {

                foreach (Match last in lastMatch.Matches(clubsID[i]))
                {
                    if (i == 0)
                    {
                        HomeLastDate += last.Groups[0].ToString() + "\n";
                        break;
                    }
                    if (i == 1)
                    {
                        VisitorsLastDate += last.Groups[0].ToString() + "\n";
                        break;
                    }
                }
                foreach (Match m in result.Matches(clubsID[i]))
                {
                    string spaceCharacterCheck = "";
                    spaceCharacterCheck = m.Groups[1].ToString();

                    if (cups.IsMatch(m.Groups[4].ToString()) && (m.Groups[2].ToString() == "win" || m.Groups[5].ToString() == "draw" || m.Groups[2].ToString() == "lose")
                        && i == 0 && HomeCount == 0)
                    {
                        HomeCups = "Cups last Played\n";
                    }

                    if (cups.IsMatch(m.Groups[4].ToString()) && (m.Groups[2].ToString() == "win" || m.Groups[2].ToString() == "draw" || m.Groups[2].ToString() == "lose")
                        && i == 1 && VisitorsCount == 0)
                    {
                        VisitorsCups = "Cups last Played\n";
                    }

                    //Home results has been found
                    if (!cups.IsMatch(m.Groups[4].ToString()) && !spaceCharacter.IsMatch(spaceCharacterCheck) &&
                        (m.Groups[2].ToString() == "win" || m.Groups[2].ToString() == "draw" || m.Groups[2].ToString() == "lose") && i == 0)
                    {
                        if (HomeCount < 10)
                        {
                            if (m.Groups[2].ToString() == "win") homeWin++;
                            else if (m.Groups[2].ToString() == "draw") homeDraw++;
                            else if (m.Groups[2].ToString() == "lose") homeLose++;
                            HomeCount++;
                        }
                    }

                    //Visitors results has been found
                    if (!cups.IsMatch(m.Groups[4].ToString()) && spaceCharacter.IsMatch(spaceCharacterCheck) &&
                        (m.Groups[2].ToString() == "win" || m.Groups[2].ToString() == "draw" || m.Groups[2].ToString() == "lose") && i == 1)
                    {
                        if (VisitorsCount < 10)
                        {
                            if (m.Groups[2].ToString() == "win") visitorsWin++;
                            else if (m.Groups[2].ToString() == "draw") visitorsDraw++;
                            else if (m.Groups[2].ToString() == "lose") visitorsLose++;
                            VisitorsCount++;
                        }
                    }
                }
            }
                //return ifs
                if (homeWin >= (homeDraw + homeLose) && visitorsLose >= (visitorsWin + visitorsDraw))
                {
                    probability = (((float)homeWin + (float)visitorsLose) / 20f) * 100f;
                    
                    //awful syntax. Gonna fix it someday!
                    return string.Format("Home Wins({0})\nHome Draws({1})\nHome Lose({2})\n\nVisitors Lose({3})\nVisitors Draws({4})\nVisitors Wins({5})\nOdds: {6}\nSo far: {7}%\nLast played: {8}Last played: {9}\n{10}\n{11}\n",
                                        homeWin, homeDraw, homeLose, visitorsLose, visitorsDraw, visitorsWin, winOdds, probability,
                                        HomeLastDate, VisitorsLastDate, HomeCups, VisitorsCups);
                }

                else return null;
        }
    }
}
