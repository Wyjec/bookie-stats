﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;

namespace Bookie_stats
{
    class MatchStatsByOdds : PredictionsByUserInput
    {
        static Regex _searchByOdds;

        public MatchStatsByOdds()
        {
            _searchByOdds = new Regex(@"<a href='../kluby_pilkarskie/porownanie/(.*?)'.*?(\d{1}\.\d{2}?)<.*?(\d{1}\.\d{2}?)<.*?(\d{1}\.\d{2}?)<.*?(\d{1}\.\d{2}?)<.*?(\d{1}\.\d{2}?)<", RegexOptions.IgnoreCase);
        }

        public new void CreatingListOfAllMatches()
        {
            if (RequiredDiff > QuantityOfLastMatches)
            {
                throw new Exception("Out of range!");
            }
            string url = "http://www.mecz91.com/zapowiedzi/" + date;
            string content = (new WebClient().DownloadString(url));

            foreach (Match m in _searchByOdds.Matches(content))
            {
                string tempUnder = m.Groups[5].ToString();
                string tempOver = m.Groups[6].ToString();


                double under = double.Parse(tempUnder, CultureInfo.InvariantCulture);
                double over = double.Parse(tempOver, CultureInfo.InvariantCulture);


                if (under < 1.60d || over < 1.60d)
                    AllMatches.Add("http://www.mecz91.com/kluby_pilkarskie/porownanie/" + m.Groups[1].ToString());
            }
        }

        protected override string OverUnderStats(string H2H, string[] clubsID)
        {
            Regex lastMatch = new Regex(@"[0-9]{2}\.[0-9]{2}\.[0-9]{4}", RegexOptions.IgnoreCase);
            string HomeLastDate = "Last Match: ";
            string VisitorsLastDate = "Last Match: ";
            string HomeHistory = "Home: ";
            string VisitorsHistory = "Visitors: ";
            string HomeCups = "";
            string VisitorsCups = "";

            int TotalMatchesCount = 0;
            int HomeCount = 0;
            int VisitorsCount = 0;

            int HomeLast5MatchesOver = 0;
            int HomeLast5MatchesUnder = 0;

            int VisitorsLast5MatchesOver = 0;
            int VisitorsLast5MatchesUnder = 0;

            int HomeOver = 0;
            int HomeUnder = 0;

            int VisitorsOver = 0;
            int VisitorsUnder = 0;

            LastMatchesProbability = 0.0f;
            probability = 0.0f;

            for (int i = 0; i <= 1; i++)
            {
                foreach (Match last in lastMatch.Matches(clubsID[i]))
                {
                    if (i == 0)
                    {
                        HomeLastDate += last.Groups[0].ToString() + "\n";
                        break;
                    }
                    if (i == 1)
                    {
                        VisitorsLastDate += last.Groups[0].ToString() + "\n";
                        break;
                    }

                }
                foreach (Match m in Under_Over_rgx.Matches(clubsID[i]))
                {
                    string name = "";
                    name = m.Groups[1].ToString();

                    if (cups.IsMatch(m.Groups[6].ToString()) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 0
                        && HomeLast5MatchesOver == 0 && HomeLast5MatchesUnder == 0)
                    {
                        HomeCups = "Cups last Played\n";
                    }

                    if (cups.IsMatch(m.Groups[6].ToString()) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 1
                        && VisitorsLast5MatchesUnder == 0 && VisitorsLast5MatchesOver == 0)
                    {
                        VisitorsCups = "Cups last Played\n";
                    }

                    //Home results has been found
                    if (!cups.IsMatch(m.Groups[6].ToString()) && !spaceCharacter.IsMatch(name) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 0)
                    {
                        if (m.Groups[5].ToString() == "U") HomeUnder++;
                        else if (m.Groups[5].ToString() == "O") HomeOver++;
                        if (HomeCount < QuantityOfLastMatches)
                        {
                            if (m.Groups[5].ToString() == "U") HomeLast5MatchesUnder++;
                            else if (m.Groups[5].ToString() == "O") HomeLast5MatchesOver++;
                            HomeHistory += m.Groups[5].ToString() + " ";
                            HomeCount++;
                        }
                        TotalMatchesCount++;
                    }

                    //Visitors results has been found
                    if (!cups.IsMatch(m.Groups[6].ToString()) && spaceCharacter.IsMatch(name) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 1)
                    {
                        if (m.Groups[5].ToString() == "U")
                            VisitorsUnder++;
                        else if (m.Groups[5].ToString() == "O") VisitorsOver++;
                        if (VisitorsCount < QuantityOfLastMatches)
                        {
                            if (m.Groups[5].ToString() == "U") VisitorsLast5MatchesUnder++;
                            else if (m.Groups[5].ToString() == "O") VisitorsLast5MatchesOver++;
                            VisitorsHistory += m.Groups[5].ToString() + " ";
                            VisitorsCount++;
                        }
                        TotalMatchesCount++;
                    }
                }
            } //for

            double under = 0.0d;
            double over = 0.0d;
            if (UnderOdds.Substring(7, 4) != "NULL")
                under = double.Parse(UnderOdds.Substring(7, 4), CultureInfo.InvariantCulture);
            if (overOdds.Substring(7, 4) != "NULL")
                over = double.Parse(overOdds.Substring(7, 4), CultureInfo.InvariantCulture);

            //Return Ifs
            if (under < 1.60d && under != 0)
            {
                LastMatchesProbability = (((float)HomeLast5MatchesUnder + (float)VisitorsLast5MatchesUnder) / (float)(QuantityOfLastMatches * 2)) * 100f;
                probability = (((float)HomeUnder + (float)VisitorsUnder) / (TotalMatchesCount)) * 100f;
                return String.Format("UNDER ({13}).\n Hosts (UNDER: {0}, OVER: {1} ).  Visitors (UNDER: {2}, OVER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                                     HomeLast5MatchesUnder, HomeLast5MatchesOver, VisitorsLast5MatchesUnder, VisitorsLast5MatchesOver, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                                     H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, UnderOdds);
            }
            else if (over < 1.60d && over != 0)
            {
                LastMatchesProbability = (((float)HomeLast5MatchesOver + (float)VisitorsLast5MatchesOver) / (float)(QuantityOfLastMatches * 2)) * 100f;
                probability = (((float)HomeOver + (float)VisitorsOver) / (float)(TotalMatchesCount)) * 100f;
                return String.Format("OVER ({13}).\n Hosts (OVER: {0}, UNDER: {1} ).  Visitors (OVER: {2}, UNDER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                     HomeLast5MatchesOver, HomeLast5MatchesUnder, VisitorsLast5MatchesOver, VisitorsLast5MatchesUnder, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                     H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, overOdds);
            }

            else return null;

        } //OverUnderStats

    }
}
