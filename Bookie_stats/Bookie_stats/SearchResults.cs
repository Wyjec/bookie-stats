﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace Bookie_stats
{

    class SearchResults
    {
        public static string date = "";
        string _resultPath = "";
        string _predictionsPath = "";
        string []_paths;

        public void GetResults()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            _predictionsPath = string.Format(@"{0}\predictions {1}.txt", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), date); ;
            _resultPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\result.txt";
            _paths = new string[] {_predictionsPath, _resultPath };
            string url = File.ReadAllText(_predictionsPath);
            List<string> urls = new List<string>();
            string file = "";
            Regex rgx = new Regex("http(.*?)\n", RegexOptions.IgnoreCase);
            Regex Under_Over_rgx = new Regex(@"(\d{2}\.\d{2}\.\d{4}?).*?showClubMenu(.*?)=(.*?)=(.*?)=(.*?)>(.*?)<(.*?)=", RegexOptions.IgnoreCase);
            string result = "";

            foreach (Match match in rgx.Matches(url))
            {
                urls.Add(match.Groups[0].ToString());
            }

            foreach (string names in urls)
            {
                file += names;
            }

            WebClient wc = new WebClient();

            foreach (string names in urls)
            {
                bool existMatch = false;
                string page = wc.DownloadString(names);
                foreach (Match match in Under_Over_rgx.Matches(page))
                {
                    string temp = "";
                    if (match.Groups[6].ToString() == "U" || match.Groups[6].ToString() == "O")
                    {
                        if (match.Groups[1].ToString() == GetPredictionsPath(date))
                        {
                            temp = match.Groups[6].ToString() + System.Environment.NewLine;
                            dic.Add(names, temp);
                            existMatch = true;
                            break;
                        }
                    }
                }
                if (!existMatch)
                    dic.Add(names, "NULL" + System.Environment.NewLine);

            }

            foreach (var dictio in dic)
            {
                result += dictio.Key + dictio.Value;
            }

            File.WriteAllText(_resultPath, result);
            AddList();
        }

        string GetPredictionsPath(string date)
        {
            Match changeDate = Regex.Match(date, @"(\d{4})-(\d{2})-(\d{2})", RegexOptions.IgnoreCase);
            string newDate = string.Format("{0}.{1}.{2}", changeDate.Groups[3].Value, changeDate.Groups[2].Value, changeDate.Groups[1].Value);
            return newDate;
        }


        void GetData(string[] paths ,params Dictionary<string, string>[] dictionaries)
        {
            int i = 0;
            foreach(var dics in dictionaries)
            {
                using (var r = new StreamReader(paths[i]))
                {
                    if (!File.Exists(paths[i]))
                        throw new Exception("File Doesn't exist");

                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        dics.Add(line, r.ReadLine());
                    }

                }
                i++;
            }
        }

        private void AddList()
        {
            string message = "";
            Dictionary<string, string> results = new Dictionary<string, string>();
            Dictionary<string, string> predicts = new Dictionary<string, string>();
            GetData(_paths, predicts, results);

            int good = 0;
            int bad = 0;
            int notPlayed = 0;
            var finalResults = results.ToDictionary(x => x.Key, x => {
                if (x.Value == "NULL") { notPlayed++; return "Not Played"; }
                else if (predicts[x.Key] == x.Value) { good++; return "Trafiony"; }
                else { bad++; return "Nie trafiony"; }
            });

            foreach (var d in finalResults)
            {

                message += d.Key + "\n" + d.Value + "\n";

            }
            message += "\nGood: " + good + "\nBad: " + bad + "\nNot Played: " + notPlayed;

            try
            {
                Sendmail.SendingResults(message);
            }

            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

            finally
            {
                foreach(var files in _paths)
                {
                    File.Delete(files);
                }
            }

        }
       
    }
}

