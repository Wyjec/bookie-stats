﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bookie_stats
{
    public partial class Form1 : Form
    {
        int check = 0;
        Regex spaceCharacterValidation = new Regex(@"\s", RegexOptions.IgnoreCase);
        Regex dateValidation = new Regex(@"(\d{4})-(\d{2})-(\d{2})$", RegexOptions.IgnoreCase);
        private ToolTip toolTip = new ToolTip();
        private bool isShown = false;
        delegate void OptionChoice();
        OptionChoice Choice;

        void ChangeLabelsState(bool change)
        {
            QuantityBox.Visible = change;
            QuantityBox.Enabled = change;
            RequiredDiffBox.Visible = change;
            RequiredDiffBox.Enabled = change;
            QuantityLabel.Visible = change;
            RequiredDiffLabel.Visible = change;
            OpenWeb.Visible = change;
            OpenWebLabel.Visible = change;
            OpenWebBox.Visible = change;
            OpenWeb.Checked = false;

        }

        void PredictionsExecute()
        {
				PredictionsByUserInput predictionByUser = new PredictionsByUserInput();
                predictionByUser.CreatingListOfAllMatches();
                predictionByUser.Execute();
        }

        void StatisticsByOdds()
        {
				MatchStatsByOdds StatsByOdds = new MatchStatsByOdds();
                StatsByOdds.CreatingListOfAllMatches();
                StatsByOdds.Execute();
        }

        void SearchResultsExecute()
        {
				SearchResults searchResults = new SearchResults();
                searchResults.GetResults();       
        }

        void ExactResult()
        {
            mainType main = new mainType();
            main.CreatingListOfAllMatches();
            main.Execute();
        }

        public Form1()
        {
            InitializeComponent();
            toolTip.InitialDelay = 0;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            if(comboBox1.SelectedIndex == 0 || comboBox1.SelectedIndex == 1)
                ChangeLabelsState(true);
            else
                ChangeLabelsState(false);

            if (comboBox1.SelectedIndex == 0)
            {
                Choice = PredictionsExecute;
            }

            else if (comboBox1.SelectedIndex == 1)
            {
                Choice = StatisticsByOdds;
            }

            else if (comboBox1.SelectedIndex == 2)
            {
                Choice = ExactResult;
            }

            else if (comboBox1.SelectedIndex == 3)
            {
                Choice = SearchResultsExecute;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (dateValidation.IsMatch(textBox1.Text))
                ExecuteButton.Enabled = true;
            else ExecuteButton.Enabled = false;

            if (!spaceCharacterValidation.IsMatch(textBox1.Text))
            {
                PredictionsByUserInput.date = textBox1.Text;
                SearchResults.date = textBox1.Text;
            }
            else textBox1.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox1.Text))
            {
                MessageBox.Show("You must pick an option!");
                return;
            }
            ExecuteButton.Enabled = false;
            OpenWeb.Enabled = false;

            backgroundWorker1.RunWorkerAsync();

        }

        private void QuantityBox_TextChanged(object sender, EventArgs e)
        {
            if (QuantityBox.Text != "" && int.TryParse(QuantityBox.Text, out check) && !spaceCharacterValidation.IsMatch(QuantityBox.Text))
                PredictionsByUserInput.QuantityOfLastMatches = int.Parse(QuantityBox.Text);
            else QuantityBox.Text = "";
        }

        private void RequiredDiffBox_TextChanged(object sender, EventArgs e)
        {
            if(RequiredDiffBox.Text != "" && int.TryParse(RequiredDiffBox.Text, out check) && !spaceCharacterValidation.IsMatch(RequiredDiffBox.Text))
            PredictionsByUserInput.RequiredDiff = int.Parse(RequiredDiffBox.Text);
            else QuantityBox.Text = "";
        }


        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if(!backgroundWorker1.IsBusy)
            {
                if (ExecuteButton == this.GetChildAtPoint(e.Location))
                {
                    if (!isShown)
                    {
                        toolTip.Show("Type date to enable", this, e.Location);
                        isShown = true;
                    }
                }
                else
                {
                    toolTip.Hide(ExecuteButton);
                    isShown = false;
                }
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Choice();
                MessageBox.Show("Statistics have been sent to your e-mail");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExecuteButton.Enabled = true;
            OpenWeb.Enabled = true;
        }

        private void OpenWeb_CheckedChanged(object sender, EventArgs e)
        {
            if(OpenWeb.Checked)
            {
                PredictionsByUserInput.openWeb = true;
                OpenWebBox.Enabled = true;
            }
            else
            {
                PredictionsByUserInput.openWeb = false;
                OpenWebBox.Enabled = false;
            }
        }

        private void OpenWebBox_TextChanged(object sender, EventArgs e)
        {
            if (OpenWebBox.Text != "" && int.TryParse(OpenWebBox.Text, out check) && !spaceCharacterValidation.IsMatch(OpenWebBox.Text)
                && int.Parse(OpenWebBox.Text) >= 0 && int.Parse(OpenWebBox.Text) <= 100)
                PredictionsByUserInput.desireProbabilityToOpenWeb = int.Parse(OpenWebBox.Text);
            else OpenWebBox.Text = "";
        }

    }
}
