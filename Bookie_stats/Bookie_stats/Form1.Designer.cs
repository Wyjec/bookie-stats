﻿namespace Bookie_stats
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExecuteButton = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.Label();
            this.RequiredDiffBox = new System.Windows.Forms.TextBox();
            this.QuantityBox = new System.Windows.Forms.TextBox();
            this.QuantityLabel = new System.Windows.Forms.Label();
            this.RequiredDiffLabel = new System.Windows.Forms.Label();
            this.date_label = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.OpenWeb = new System.Windows.Forms.CheckBox();
            this.OpenWebLabel = new System.Windows.Forms.Label();
            this.OpenWebBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ExecuteButton
            // 
            this.ExecuteButton.Enabled = false;
            this.ExecuteButton.Location = new System.Drawing.Point(199, 214);
            this.ExecuteButton.Name = "ExecuteButton";
            this.ExecuteButton.Size = new System.Drawing.Size(75, 23);
            this.ExecuteButton.TabIndex = 0;
            this.ExecuteButton.Text = "Execute!";
            this.ExecuteButton.UseVisualStyleBackColor = true;
            this.ExecuteButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Predictions (Under/Over)",
            "Stats by Odds",
            "Predictions (Result)",
            "Results of Predictions"});
            this.comboBox1.Location = new System.Drawing.Point(12, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(136, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(181, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(120, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Option";
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Location = new System.Drawing.Point(191, 7);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(107, 13);
            this.Date.TabIndex = 5;
            this.Date.Text = "Date (YYYY-MM-DD)";
            // 
            // RequiredDiffBox
            // 
            this.RequiredDiffBox.Enabled = false;
            this.RequiredDiffBox.Location = new System.Drawing.Point(12, 110);
            this.RequiredDiffBox.Name = "RequiredDiffBox";
            this.RequiredDiffBox.Size = new System.Drawing.Size(100, 20);
            this.RequiredDiffBox.TabIndex = 6;
            this.RequiredDiffBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.RequiredDiffBox.Visible = false;
            this.RequiredDiffBox.TextChanged += new System.EventHandler(this.RequiredDiffBox_TextChanged);
            // 
            // QuantityBox
            // 
            this.QuantityBox.Enabled = false;
            this.QuantityBox.Location = new System.Drawing.Point(12, 70);
            this.QuantityBox.Name = "QuantityBox";
            this.QuantityBox.Size = new System.Drawing.Size(100, 20);
            this.QuantityBox.TabIndex = 7;
            this.QuantityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.QuantityBox.Visible = false;
            this.QuantityBox.TextChanged += new System.EventHandler(this.QuantityBox_TextChanged);
            // 
            // QuantityLabel
            // 
            this.QuantityLabel.AutoSize = true;
            this.QuantityLabel.Location = new System.Drawing.Point(12, 54);
            this.QuantityLabel.Name = "QuantityLabel";
            this.QuantityLabel.Size = new System.Drawing.Size(149, 13);
            this.QuantityLabel.TabIndex = 8;
            this.QuantityLabel.Text = "Quantity Of The Last Matches";
            this.QuantityLabel.Visible = false;
            // 
            // RequiredDiffLabel
            // 
            this.RequiredDiffLabel.AutoSize = true;
            this.RequiredDiffLabel.Location = new System.Drawing.Point(12, 94);
            this.RequiredDiffLabel.Name = "RequiredDiffLabel";
            this.RequiredDiffLabel.Size = new System.Drawing.Size(129, 13);
            this.RequiredDiffLabel.TabIndex = 9;
            this.RequiredDiffLabel.Text = "Required Stats Difference";
            this.RequiredDiffLabel.Visible = false;
            // 
            // date_label
            // 
            this.date_label.AutoSize = true;
            this.date_label.Location = new System.Drawing.Point(196, 198);
            this.date_label.Name = "date_label";
            this.date_label.Size = new System.Drawing.Size(102, 13);
            this.date_label.TabIndex = 10;
            this.date_label.Text = "Type date to enable";
            this.date_label.Visible = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // OpenWeb
            // 
            this.OpenWeb.AutoSize = true;
            this.OpenWeb.Location = new System.Drawing.Point(15, 150);
            this.OpenWeb.Name = "OpenWeb";
            this.OpenWeb.Size = new System.Drawing.Size(106, 17);
            this.OpenWeb.TabIndex = 11;
            this.OpenWeb.Text = "Open Match Site";
            this.OpenWeb.UseVisualStyleBackColor = true;
            this.OpenWeb.Visible = false;
            this.OpenWeb.CheckedChanged += new System.EventHandler(this.OpenWeb_CheckedChanged);
            // 
            // OpenWebLabel
            // 
            this.OpenWebLabel.AutoSize = true;
            this.OpenWebLabel.Location = new System.Drawing.Point(12, 170);
            this.OpenWebLabel.Name = "OpenWebLabel";
            this.OpenWebLabel.Size = new System.Drawing.Size(124, 13);
            this.OpenWebLabel.TabIndex = 12;
            this.OpenWebLabel.Text = "% with at least probability";
            this.OpenWebLabel.Visible = false;
            // 
            // OpenWebBox
            // 
            this.OpenWebBox.Enabled = false;
            this.OpenWebBox.Location = new System.Drawing.Point(12, 186);
            this.OpenWebBox.Name = "OpenWebBox";
            this.OpenWebBox.Size = new System.Drawing.Size(33, 20);
            this.OpenWebBox.TabIndex = 13;
            this.OpenWebBox.Visible = false;
            this.OpenWebBox.TextChanged += new System.EventHandler(this.OpenWebBox_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 249);
            this.Controls.Add(this.OpenWebBox);
            this.Controls.Add(this.OpenWebLabel);
            this.Controls.Add(this.OpenWeb);
            this.Controls.Add(this.date_label);
            this.Controls.Add(this.RequiredDiffLabel);
            this.Controls.Add(this.QuantityLabel);
            this.Controls.Add(this.QuantityBox);
            this.Controls.Add(this.RequiredDiffBox);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.ExecuteButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Bookie stats";
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExecuteButton;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.TextBox RequiredDiffBox;
        private System.Windows.Forms.TextBox QuantityBox;
        private System.Windows.Forms.Label QuantityLabel;
        private System.Windows.Forms.Label RequiredDiffLabel;
        private System.Windows.Forms.Label date_label;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckBox OpenWeb;
        private System.Windows.Forms.Label OpenWebLabel;
        private System.Windows.Forms.TextBox OpenWebBox;
    }
}

