﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;

namespace Bookie_stats
{
    class PredictionsByUserInput
    {
        //Regex ignores all cups
        protected readonly Regex cups;
        //Regex searches for club ID
        protected readonly Regex club_ID_rgx;
        //Regex searches for space chars
        protected readonly Regex spaceCharacter;
        //Regex searches for Under/Over Informations
        protected readonly Regex Under_Over_rgx;
        //Regex searches all matches
        readonly Regex dailyMatches;
        //Regex eliminates not wanted Competitions
        protected readonly Regex CompetitionSelect;

        DateTime localDate = DateTime.Now;
        protected List<string> AllMatches;
        public Dictionary<string, string> MatchIDs { private set; get; }
        protected string filePath;
        // strings hold the stakes
        protected string overOdds;
        protected string UnderOdds;
        protected string winOdds;

        public static string date;
        protected string message;

        protected float LastMatchesProbability;
        protected float probability;

        // pattern eliminating not wanted matches
        protected string cupsPattern;
        // Here I take a year that is passed to the funcion which checks the entire current year stats for the Homes and the Visitors. If it's January, it takes last year
        protected string yearOfStats;
        public static int QuantityOfLastMatches = 5;
        public static int RequiredDiff = 0;
        public static int desireProbabilityToOpenWeb = 0;

        public static bool openWeb= false;

        //point to RetvieveContent function depending on current month. If it's january or february, it takes the current and the last year stats.
        protected delegate string Retrieve(string h2h, params string[] clubsID);
        protected Retrieve RetrieveContent;


        public PredictionsByUserInput()
        {
            if (localDate.Month >= 04)
            {
                yearOfStats = localDate.Year.ToString();
                RetrieveContent = RetrieveContentCurrentYear;
            }
            else
            {
                yearOfStats = localDate.AddYears(-1).Year.ToString();
                RetrieveContent = RetrieveContentLastYear;
            }

            cupsPattern = "Puchar|Play|Liga|Copa|Towarzyskie";
            //Regexes
            club_ID_rgx = new Regex("club_id=(.*?)&", RegexOptions.IgnoreCase);
            spaceCharacter = new Regex("nbsp", RegexOptions.IgnoreCase);
            Under_Over_rgx = new Regex(@"showClubMenu(.*?)=(.*?)=(.*?)=(.*?)>(.*?)<(.*?)=", RegexOptions.IgnoreCase);
            dailyMatches = new Regex(@"<a href='../kluby_pilkarskie/porownanie/(.*?)'", RegexOptions.IgnoreCase);
            CompetitionSelect = new Regex(@"<img src='http://www.mecz91.com/shared/images/flags/.*?;(.*?)<", RegexOptions.IgnoreCase);
            cups = new Regex(cupsPattern, RegexOptions.IgnoreCase);
            //Regexes

            //Dictionary with match links and stats
            MatchIDs = new Dictionary<string, string>();

            // File goes to My Documents folder
            filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            //List that holds all daily matches links
            AllMatches = new List<string>();
            message = overOdds = UnderOdds = winOdds = "";
        }

        // Function gathers entire daily matches
        public void CreatingListOfAllMatches()
        {

            // Exit if it's greater than QuantityOfLastMatches
            if (RequiredDiff > QuantityOfLastMatches)
            {
                throw new Exception("Out of range!");
            }

            string url = "http://www.mecz91.com/zapowiedzi/" + date;
            string content = (new WebClient().DownloadString(url));

            foreach (Match m in dailyMatches.Matches(content))
            {

                AllMatches.Add("http://www.mecz91.com/kluby_pilkarskie/porownanie/" + m.Groups[1].ToString());
            }
            if (AllMatches.Count == 0)
                throw new Exception("Ups. Zero matches Today?");

        }
        public void Execute()
        {
            SearchHomeAndVisitorClubID();
            ShowStats();
        }

        //Function searches clubs ID from a single URL, then put them into 2 strings
        protected virtual void SearchHomeAndVisitorClubID()
        {

            foreach (var Matches in AllMatches)
            {
                //Club ID's
                string FirstClubID = "";
                string SecondClubID = "";
                string content = (new WebClient().DownloadString(Matches));
                if (MatchSelections(content)) continue;
                Match BetweenRgx = Regex.Match(content, @"Mecze bezpo.*Sezon(.*?)Wyniki", RegexOptions.IgnoreCase);
                string check = BetweenRgx.Groups[1].Value;
                string H2H = "H2H: ";

                //Searching for the H2H matches only for home matches
                foreach (var temp in ComparisionH2H(BetweenRgx.Groups[1].Value))
                {
                    H2H += temp;
                }
                if (H2H == "H2H: ") H2H = "H2H: Never Played before";


                foreach (Match m in club_ID_rgx.Matches(content))
                {
                    if (FirstClubID != "" && SecondClubID != "")
                    {
                        string tempString;
                        if ((tempString = RetrieveContent(H2H, FirstClubID, SecondClubID)) == null)
                            break;
                        else
                        {
                             if(openWeb && probability >= desireProbabilityToOpenWeb)
                                 System.Diagnostics.Process.Start(Matches);

                            MatchIDs.Add(Matches, tempString);
                            break;
                        }

                    }

                    if (FirstClubID == "")
                    {
                        FirstClubID = m.Groups[1].ToString();
                    }
                    else if (FirstClubID != m.Groups[1].ToString() && SecondClubID == "")
                    {
                        SecondClubID = m.Groups[1].ToString();
                    }
                }

            }
        }
        //Function devides one match into a 2 seperate URL's. 1 for the Homes stats, second for the Visitors stats
        protected string RetrieveContentCurrentYear(string h2h, params string[] clubsID)
        {

            //Array that holds home and visitors HTML code
            string[] HomeVisitorsURL = new string[2];
            int i = 0;
            foreach (var IDs in clubsID)
            {
                using (WebClient client = new WebClient())
                {

                    System.Collections.Specialized.NameValueCollection reqparm = new System.Collections.Specialized.NameValueCollection();
                    reqparm.Add("club_id", IDs);
                    reqparm.Add("prefix", "../../");
                    reqparm.Add("container", "home_last_results");
                    reqparm.Add("year", yearOfStats);
                    byte[] responsebytes = client.UploadValues("http://mecz91.com/ajax/last_results.php", "POST", reqparm);
                    HomeVisitorsURL[i] = Encoding.UTF8.GetString(responsebytes);
                    i++;
                }
            }
            return OverUnderStats(h2h, HomeVisitorsURL);
        }

        protected string RetrieveContentLastYear(string h2h, params string[] clubsID)
        {
            int year = localDate.Year;
            //Array that holds home and visitors HTML code
            string[] HomeVisitorsURL = new string[2];

            int i = 0;
            foreach (var IDs in clubsID)
            {
                using (WebClient client = new WebClient())
                {

                    System.Collections.Specialized.NameValueCollection reqparm = new System.Collections.Specialized.NameValueCollection();
                    reqparm.Add("club_id", IDs);
                    reqparm.Add("prefix", "../../");
                    reqparm.Add("container", "home_last_results");
                    for (int c = 0; c < 1; c++)
                    {
                        reqparm.Add("year", year.ToString());
                        byte[] responsebytes = client.UploadValues("http://mecz91.com/ajax/last_results.php", "POST", reqparm);
                        HomeVisitorsURL[i] = Encoding.UTF8.GetString(responsebytes);

                    }
                    reqparm.Remove("year");
                    reqparm.Add("year", yearOfStats);
                    byte[] responsebytes2 = client.UploadValues("http://mecz91.com/ajax/last_results.php", "POST", reqparm);
                    HomeVisitorsURL[i] += Encoding.UTF8.GetString(responsebytes2);

                    i++;
                }
            }
            return OverUnderStats(h2h, HomeVisitorsURL);
        }


        // All magic happens here. Count of the goals for home and visitors.
        protected virtual string OverUnderStats(string H2H, string[] clubsID)
        {
            Regex lastMatch = new Regex(@"[0-9]{2}\.[0-9]{2}\.[0-9]{4}", RegexOptions.IgnoreCase);
            string HomeLastDate = "Last Match: ";
            string VisitorsLastDate = "Last Match: ";
            string HomeHistory = "Home: ";
            string VisitorsHistory = "Visitors: ";
            string HomeCups = "";
            string VisitorsCups = "";

            int TotalMatchesCount = 0;
            int HomeCount = 0;
            int VisitorsCount = 0;

            int HomeLast5MatchesOver = 0;
            int HomeLast5MatchesUnder = 0;

            int VisitorsLast5MatchesOver = 0;
            int VisitorsLast5MatchesUnder = 0;

            int HomeOver = 0;
            int HomeUnder = 0;

            int VisitorsOver = 0;
            int VisitorsUnder = 0;

            LastMatchesProbability = 0.0f;
            probability = 0.0f;


            for (int i = 0; i <= 1; i++)
            {
                foreach (Match last in lastMatch.Matches(clubsID[i]))
                {
                    if (i == 0)
                    {
                        HomeLastDate += last.Groups[0].ToString() + "\n";
                        break;
                    }
                    if (i == 1)
                    {
                        VisitorsLastDate += last.Groups[0].ToString() + "\n";
                        break;
                    }

                }
                foreach (Match m in Under_Over_rgx.Matches(clubsID[i]))
                {
                    string name = "";
                    name = m.Groups[1].ToString();

                    if (cups.IsMatch(m.Groups[6].ToString()) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 0
                        && HomeLast5MatchesOver == 0 && HomeLast5MatchesUnder == 0)
                    {
                        HomeCups = "Cups last Played\n";
                    }

                    if (cups.IsMatch(m.Groups[6].ToString()) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 1
                        && VisitorsLast5MatchesUnder == 0 && VisitorsLast5MatchesOver == 0)
                    {
                        VisitorsCups = "Cups last Played\n";
                    }

                    //Home results has been found
                    if (!cups.IsMatch(m.Groups[6].ToString()) && !spaceCharacter.IsMatch(name) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 0)
                    {
                        if (m.Groups[5].ToString() == "U") HomeUnder++;
                        else if (m.Groups[5].ToString() == "O") HomeOver++;
                        if (HomeCount < QuantityOfLastMatches)
                        {
                            if (m.Groups[5].ToString() == "U") HomeLast5MatchesUnder++;
                            else if (m.Groups[5].ToString() == "O") HomeLast5MatchesOver++;
                            HomeHistory += m.Groups[5].ToString() + " ";
                            HomeCount++;
                        }
                        TotalMatchesCount++;
                    }

                    //Visitors results has been found
                    if (!cups.IsMatch(m.Groups[6].ToString()) && spaceCharacter.IsMatch(name) && (m.Groups[5].ToString() == "U" || m.Groups[5].ToString() == "O") && i == 1)
                    {
                        if (m.Groups[5].ToString() == "U")
                            VisitorsUnder++;
                        else if (m.Groups[5].ToString() == "O") VisitorsOver++;
                        if (VisitorsCount < QuantityOfLastMatches)
                        {
                            if (m.Groups[5].ToString() == "U") VisitorsLast5MatchesUnder++;
                            else if (m.Groups[5].ToString() == "O") VisitorsLast5MatchesOver++;
                            VisitorsHistory += m.Groups[5].ToString() + " ";
                            VisitorsCount++;
                        }
                        TotalMatchesCount++;
                    }
                }
            } //for

            //Return Ifs
            if (((((float)HomeOver + (float)VisitorsOver) / (float)(TotalMatchesCount)) * 100f) > 80 && TotalMatchesCount > 15)
            {
                LastMatchesProbability = (((float)HomeLast5MatchesOver + (float)VisitorsLast5MatchesOver) / (float)(QuantityOfLastMatches * 2)) * 100f;
                probability = (((float)HomeOver + (float)VisitorsOver) / (float)(TotalMatchesCount)) * 100f;
                return String.Format("OVER PER YEAR ({13}).\n Hosts (OVER: {0}, UNDER: {1} ).  Visitors (OVER: {2}, UNDER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                    HomeLast5MatchesOver, HomeLast5MatchesUnder, VisitorsLast5MatchesOver, VisitorsLast5MatchesUnder, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                    H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, overOdds);
            }

            else if (((((float)HomeUnder + (float)VisitorsUnder) / (float)(TotalMatchesCount)) * 100f) > 80 && TotalMatchesCount > 15)
            {
                LastMatchesProbability = (((float)HomeLast5MatchesUnder + (float)VisitorsLast5MatchesUnder) / (float)(QuantityOfLastMatches * 2)) * 100f;
                probability = (((float)HomeUnder + (float)VisitorsUnder) / (float)(TotalMatchesCount)) * 100f;
                return String.Format("UNDER PER YEAR ({13}).\n Hosts (UNDER: {0}, OVER: {1} ).  Visitors (UNDER: {2}, OVER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                    HomeLast5MatchesUnder, HomeLast5MatchesOver, VisitorsLast5MatchesUnder, VisitorsLast5MatchesOver, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                    H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, UnderOdds);
            }

            else if (HomeLast5MatchesOver > HomeLast5MatchesUnder && VisitorsLast5MatchesOver > VisitorsLast5MatchesUnder && (HomeLast5MatchesOver - HomeLast5MatchesUnder) >= RequiredDiff
                && (VisitorsLast5MatchesOver - VisitorsLast5MatchesUnder) >= RequiredDiff)
            {
                LastMatchesProbability = (((float)HomeLast5MatchesOver + (float)VisitorsLast5MatchesOver) / (float)(QuantityOfLastMatches * 2)) * 100f;
                probability = (((float)HomeOver + (float)VisitorsOver) / (float)(TotalMatchesCount)) * 100f;
                return String.Format("OVER ({13}).\n Hosts (OVER: {0}, UNDER: {1} ).  Visitors (OVER: {2}, UNDER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                    HomeLast5MatchesOver, HomeLast5MatchesUnder, VisitorsLast5MatchesOver, VisitorsLast5MatchesUnder, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                    H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, overOdds);
            }

            else if (HomeLast5MatchesOver < HomeLast5MatchesUnder && VisitorsLast5MatchesOver < VisitorsLast5MatchesUnder && (HomeLast5MatchesUnder - HomeLast5MatchesOver) >= RequiredDiff
                     && (VisitorsLast5MatchesUnder - VisitorsLast5MatchesOver) >= RequiredDiff)
            {
                LastMatchesProbability = (((float)HomeLast5MatchesUnder + (float)VisitorsLast5MatchesUnder) / (float)(QuantityOfLastMatches * 2)) * 100f;
                probability = (((float)HomeUnder + (float)VisitorsUnder) / (TotalMatchesCount)) * 100f;
                return String.Format("UNDER ({13}).\n Hosts (UNDER: {0}, OVER: {1} ).  Visitors (UNDER: {2}, OVER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                    HomeLast5MatchesUnder, HomeLast5MatchesOver, VisitorsLast5MatchesUnder, VisitorsLast5MatchesOver, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                    H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, UnderOdds);
            }

            else if (((((float)HomeLast5MatchesUnder) / (float)(QuantityOfLastMatches)) * 100f > 95 || (((float)VisitorsLast5MatchesUnder) / (float)(QuantityOfLastMatches)) * 100f > 95)
                    && QuantityOfLastMatches >= 4 && (LastMatchesProbability = (((float)HomeLast5MatchesOver + (float)VisitorsLast5MatchesOver) / (float)(QuantityOfLastMatches * 2)) * 100f) >= 80)
            {
                probability = (((float)HomeUnder + (float)VisitorsUnder) / (TotalMatchesCount)) * 100f;
                return String.Format("UNDER BY TEAM ({13}).\n Hosts (UNDER: {0}, OVER: {1} ).  Visitors (UNDER: {2}, OVER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                    HomeLast5MatchesUnder, HomeLast5MatchesOver, VisitorsLast5MatchesUnder, VisitorsLast5MatchesOver, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                    H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, UnderOdds);
            }

            else if (((((float)HomeLast5MatchesOver) / (float)(QuantityOfLastMatches)) * 100f > 95 || (((float)VisitorsLast5MatchesOver) / (float)(QuantityOfLastMatches)) * 100f > 95)
                    && QuantityOfLastMatches >= 4 && (LastMatchesProbability = (((float)HomeLast5MatchesOver + (float)VisitorsLast5MatchesOver) / (float)(QuantityOfLastMatches * 2)) * 100f) >= 80)
            {
                probability = (((float)HomeOver + (float)VisitorsOver) / (TotalMatchesCount)) * 100f;
                return String.Format("OVER BY TEAM ({13}).\n Hosts (OVER: {0}, UNDER: {1} ).  Visitors (OVER: {2}, UNDER: {3} ).\n {4}\n{5}{6}\n{7}{8}\nSo far yearly: {9}%\nSo far Last Matches: {10}%\n{11}{12}\n",
                    HomeLast5MatchesOver, HomeLast5MatchesUnder, VisitorsLast5MatchesOver, VisitorsLast5MatchesUnder, HomeHistory, HomeLastDate, VisitorsHistory, VisitorsLastDate,
                    H2H, probability, LastMatchesProbability, HomeCups, VisitorsCups, overOdds);
            }
            else
                return null;
        } //OverUnderStats


        // Entire informations are incoming here. Then the message is created. At the end SendingMail function is called.
        protected virtual void ShowStats()
        {
            // string for the file statistics
            string toFile = "";
            int hits = 0;
            filePath += string.Format(@"\predictions {0}.txt", date);

            using (StreamWriter outputFile = File.CreateText(filePath))
            {
                foreach (var stats in MatchIDs)
                {
                    message += stats.Key + "\n" + stats.Value + "\n";
                    toFile += stats.Key + Environment.NewLine + stats.Value.Substring(0, 1).ToString() + Environment.NewLine;

                    hits++;
                }
                if (message == "")
                {
                    throw new Exception("Bad luck. Zero good matches!");
                }
                else
                    outputFile.WriteLine(toFile);
            }
            Sendmail.SendingPredictions(message, date, hits);
        } // ShowStats


        // Some H2H stats are taking from here.
        private IEnumerable<string> ComparisionH2H(string url)
        {
            Regex ComparisionH2H = new Regex(@"class='row'(.*?)class?(.*?)('over'|'under'.*?)>(.?)<", RegexOptions.IgnoreCase);

            foreach (Match m in ComparisionH2H.Matches(url))
            {
                if (!spaceCharacter.IsMatch(m.Groups[1].ToString()))
                    yield return m.Groups[4].ToString() + " ";
            }
        }

        // Get rid of the all cups, friendly matches etx.
        protected virtual bool MatchSelections(string content)
        {
            // Shouldn't be here, but never mind. Ugly regex searches for the Bet-At-Home stakes.
            Match odds = Regex.Match(content, @"Bet-at-home.*?(efefef;'>|<b>)+(.*?)<.*?(efefef;'>|<b>)+(.*?)<.*?(efefef;'>|<b>)+(.*?)<.*?(<td >|<b>)+(.*?)<.*?(<td >|<b>)+(.*?)<",
                                        RegexOptions.IgnoreCase);
                string tempUnder = odds.Groups[8].Value;
                string tempOver = odds.Groups[10].Value;
                if (tempUnder == "&nbsp;" || tempOver == "&nbsp;" || tempOver == "" || tempUnder == "")
                {
                    UnderOdds = "Stake: NULL";
                    overOdds = "Stake: NULL";
                }
                else
                {
                    UnderOdds = string.Format("Stake: {0}", tempUnder);
                    overOdds = string.Format("Stake: {0}", tempOver);

                }
            
    

            bool select = false;

            // Throws out all cups etc.
            foreach (Match elimination in CompetitionSelect.Matches(content))
            {
                if (cups.IsMatch(elimination.Groups[1].ToString()))
                {
                    select = true;
                    break;
                }
            }
            return select;
        }

    }
}

